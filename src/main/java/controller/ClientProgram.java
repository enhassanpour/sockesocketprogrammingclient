package controller;// A Java program for a ClientSide

import com.fasterxml.jackson.databind.ObjectMapper;
import model.entity.RequestBody;
import model.entity.Rscode;
import model.entity.RscodeValue;
import model.service.TerminalConfig;
import model.service.RscodeService;
import org.apache.log4j.Logger;

import java.net.*;
import java.io.*;

import java.util.ArrayList;
import java.util.List;


public class ClientProgram {


    public ClientProgram() {
        try {
            TerminalConfig terminalConfig = TerminalConfig.getInstance();
            Logger logger = Logger.getLogger(ClientProgram.class);
            logger.debug("Start Client ");
            Socket socket = new Socket(terminalConfig.getIp(), terminalConfig.getPort());
            DataInputStream din = new DataInputStream(socket.getInputStream());
            DataOutputStream dout = new DataOutputStream(socket.getOutputStream());

            List<RequestBody> requestBodyList = terminalConfig.getRequestBodies();
            ObjectMapper objectMapper = new ObjectMapper();

            for (RequestBody requestBody : requestBodyList) {
                requestBody.setRscode(Rscode.sentData);
                dout.writeUTF(objectMapper.writeValueAsString(requestBody));
                logger.info("sent one request by id : " + requestBody.getId());
                System.out.println("sent one request by id : " + requestBody.getId());
                dout.flush();
                RscodeValue rscodeValue = RscodeService.getInstance().getRscodeValue(objectMapper.readValue(din.readUTF(), RequestBody.class).getRscode());

                if (rscodeValue.getRscode() != Rscode.done) {
                    logger.error("resive response of requrst by id : " + requestBody.getId() + " With erro " + rscodeValue.toString());
                    System.out.println("resive response of requrst by id : " + requestBody.getId() + " With erro " + rscodeValue.toString());
                } else {
                    logger.info("resive response of requrst by id :" + requestBody.getId() + " Without Error" + rscodeValue.toString());
                    System.out.println("resive response of requrst by id :" + requestBody.getId() + " Without Error" + rscodeValue.toString());
                }
                requestBody.setRscode(rscodeValue.getRscode());
            }


            dout.close();
            socket.close();
            logger.debug("End Client ");
            terminalConfig.writeResponseInFile(requestBodyList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}