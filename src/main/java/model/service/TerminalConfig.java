package model.service;

import model.entity.RequestBody;
import model.entity.RequestType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TerminalConfig {
    private static TerminalConfig terminalConfig = new TerminalConfig();
    private String ip = "";
    private int port = 0;
    private List<RequestBody> requestBodies;

    private TerminalConfig() {
        Document document = readFile();
        readTerminalConfigFromFile(document);
        readTransactionFromFile(document);
    }

    public static TerminalConfig getInstance() {
        return terminalConfig;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public List<RequestBody> getRequestBodies() {
        return requestBodies;
    }

    private Document readFile() {
        Document document = null;
        try {
            File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\terminal.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return document;
        }
    }

    private void readTerminalConfigFromFile(Document document) {

        try {
            File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\terminal.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("server");
            Node node = nodeList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                port = Integer.valueOf(element.getAttribute("port"));
                ip = element.getAttribute("ip");
            }

            nodeList = document.getElementsByTagName("outlog");
            node = nodeList.item(0);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String logAddress = System.getProperty("user.dir") + "//src//main//resources//" + element.getAttribute("path");
                System.setProperty("clinetLogfile.name", logAddress);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void readTransactionFromFile(Document document) {
        NodeList nodeList = document.getElementsByTagName("transactions");
        Node node = nodeList.item(0);

        if (node.hasChildNodes())
            nodeList = node.getChildNodes();

        List<RequestBody> requestBodyList = new ArrayList<>();

        for (int i = 0; i < nodeList.getLength(); i++) {
            node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                RequestBody requestBody = new RequestBody();
                Element element = (Element) node;
                requestBody.setId(Long.valueOf(element.getAttribute("id")));
                requestBody.setRequestType(RequestType.valueOf(element.getAttribute("type")));
                requestBody.setAmount(Long.valueOf(element.getAttribute("amount")));
                requestBody.setCustomerId(element.getAttribute("deposit"));
                requestBody.setTerminalId(document.getDocumentElement().getAttribute("id"));
                requestBodyList.add(requestBody);
            }

        }
        requestBodies = requestBodyList;
    }

    public void writeResponseInFile(List<RequestBody> requestBodyList) {
        try {
            PrintWriter printWriter = new PrintWriter(System.getProperty("user.dir") + "\\src\\main\\resources\\Output.txt", "UTF-8");

            for (RequestBody requestBody : requestBodyList) {
                printWriter.println(requestBody.toString());
            }

            printWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
