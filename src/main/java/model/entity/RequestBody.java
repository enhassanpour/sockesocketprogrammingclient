package model.entity;

import model.service.RscodeService;

public class RequestBody {
    private long id;
    private String terminalId;
    private String customerId;
    private RequestType requestType;
    private long amount;
    private Rscode rscode;

    public Rscode getRscode() {
        return rscode;
    }

    public RequestBody setRscode(Rscode rscode) {
        this.rscode = rscode;
        return this;
    }

    public long getId() {
        return id;
    }

    public RequestBody setId(long id) {
        this.id = id;
        return this;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public RequestBody setTerminalId(String terminalId) {
        this.terminalId = terminalId;
        return this;
    }

    public String getCustomerId() {
        return customerId;
    }

    public RequestBody setCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public RequestBody setRequestType(RequestType requestType) {
        this.requestType = requestType;
        return this;
    }

    public long getAmount() {
        return amount;
    }

    public RequestBody setAmount(long amount) {
        this.amount = amount;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                "id :" + id +
                ", terminalId :'" + terminalId + '\'' +
                ", customerId : '" + customerId + '\'' +
                ", requestType : " + requestType +
                ", amount : " + amount +
                ", rscodeValue : " + RscodeService.getInstance().getRscodeValue(rscode).toString() +
                '}';
    }
}
